var App = App || {};
App.GifGenerator = (function () {
    "use strict";
    /* eslint-env browser, jquery  */
    /* globals EventBus */

    var video,
        frames,
        showcase,
        gifModel;

    function onGifCreated(url) {
        showcase.setShowcaseImage(url);
    }
    
    function createGIF(frameArray) {
        for (var i = 0; i<frameArray.target.length; i++) {
            gifModel.addImage(frameArray.target[i]);
        }
        gifModel.createGif(onGifCreated);
        
        
    }

    function initEventBus() {
        EventBus.addEventListener("createFrame", function (sender, source) {
            frames.addFrame(source);
        });
        EventBus.addEventListener("createGIF", createGIF);
    }

    function init() {
        video = new App.VideoController($(".video-box"));
        frames = new App.FramesView($(".frames-box"), $(".button.create-gif"));
        showcase = new App.ShowcaseView($(".gif-box"));
        gifModel = new App.GifModel({
            workerScript: "./js/libs/gifjs/gif.worker.js",
            quality: 2,
            workers: 2,
            width: 480,
            height: 360,
            delay: 500

        });
        initEventBus();
    }

    return {
        init: init
    };
}());
