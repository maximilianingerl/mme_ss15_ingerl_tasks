var App = App || {};
App.VideoController = function (container) {
    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */

    var video,
        prompt,
        frameButton,
        frame;

    function playVideo(src) {
        var videoNode = video[0];
        src = window.URL.createObjectURL(src);
        prompt.addClass("hidden");
        frameButton.removeClass("hidden");
        videoNode.src = src;
        videoNode.play();
    }

    function onDropFile(event) {
        event.stopPropagation();
        event.preventDefault();
        container.removeClass("dragover");
        container.addClass("playing");
        playVideo(event.originalEvent.dataTransfer.files[0]);
    }


    function onDragLeftFile() {
        container.removeClass("dragover");
    }

    function onDragOverFile(event) {
        event.stopPropagation();
        event.preventDefault();
        event.originalEvent.dataTransfer.dropEffect = "copy";
        container.addClass("dragover");
    }
   
    function onFrameButtonClicked() {
        var videoNode = video[0];
        EventBus.dispatch("createFrame", this, {
            video: videoNode,
            width: videoNode.width,
            time: videoNode.currentTime,
            height: videoNode.height
        });
    }


    function init() {
        video = container.find(".video");
        prompt = container.find(".video-prompt");
        frameButton = container.find(".create-frame");

        video.on("dragover", onDragOverFile);
        video.on("dragleave", onDragLeftFile);
        video.on("drop", onDropFile);
        frameButton.on("click", onFrameButtonClicked);
    }

    init();

    return {};

};
