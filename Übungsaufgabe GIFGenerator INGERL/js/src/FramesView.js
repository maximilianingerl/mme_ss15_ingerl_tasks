var App = App || {};
App.FramesView = function (container, createButton) {
    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */

    var frameList,
        hiddenCanvas,
        frame,
        switchFrameArray = [];

    function getFrameLocation(frame) {
        var frameArray = frameList.children();
        for (var temp = 0; temp < frameArray.length; temp++) {
            if(frame === frameArray.toArray()[temp].firstChild) {
                return temp;
            }
        }
    }
    
    function switchFrames() {
        if (switchFrameArray[0] === switchFrameArray[1]) {
            return;
        }
        var switchedFrame = switchFrameArray[0].firstChild,
            onDropFrame = switchFrameArray[1].firstChild,
            switchedFrameLocation = getFrameLocation(switchedFrame),
            onDropFrameLocation = getFrameLocation(onDropFrame),
            frameArray = frameList.children();
        
        frameArray.splice(switchedFrameLocation, 1);
        frameArray.splice(onDropFrameLocation, 0, switchFrameArray[0]);
        frameList.empty();
        
        for (var temp = 0; temp<frameArray.length; temp++) {
            rearrangeFrames(frameArray.toArray()[temp].firstChild);
        }
            
        
    }
    
   
    
    
    function onDragOverFrame(event) {
        event.stopPropagation();
        event.preventDefault();
        var parent = $(event.target).parent();
        parent.addClass("highlight");
    }
    
    function onDragLeaveFrame (event) {
        var parent = $(event.target).parent();
        parent.removeClass("highlight");
    }
    
    function onDropFrame (event) {
        event.stopPropagation();
        event.preventDefault();
        switchFrameArray[1] = event.currentTarget;
        var parent = $(event.target).parent();
        parent.removeClass("highlight");
        
        switchFrames();
    }
    
    function onDragStart (event) {
        switchFrameArray[0] = event.currentTarget;
    }

    function extractImage(videoSource) {
        var canvasNode = hiddenCanvas[0],
            image = new Image();
        image.height = canvasNode.height = videoSource.height;
        image.width = canvasNode.width = videoSource.width;
        canvasNode.getContext("2d").drawImage(videoSource.video, 0, 0, videoSource.width, videoSource.height);
        image.setAttribute("frame", videoSource.time.toString().replace(".", "-"));
        image.setAttribute("src", canvasNode.toDataURL("image/png"));
        return image;
    }


    function addFrame(videoSource) {
        var imageNode = extractImage(videoSource),
            listElement = $("<li></li>"),
            deleteButton = $("<span class='button delete-frame'>remove</span>");
        listElement.attr("draggable", true);
        listElement.append(imageNode);
        listElement.append(deleteButton);
        listElement.on("dragstart", onDragStart);
        listElement.on("dragover", onDragOverFrame);
        listElement.on("dragleave", onDragLeaveFrame);
        listElement.on("drop", onDropFrame);
        frameList.append(listElement);
        if (frameList.children().length > 1) {
            createButton.removeClass("disabled");
        }
    }
    
    function rearrangeFrames(frame) {
        var imageNode = frame,
            listElement = $("<li></li>"),
            deleteButton = $("<span class='button delete-frame'>remove</span>");
        listElement.attr("draggable", true);
        listElement.append(imageNode);
        listElement.append(deleteButton);
        listElement.on("dragstart", onDragStart);
        listElement.on("dragover", onDragOverFrame);
        listElement.on("dragleave", onDragLeaveFrame);
        listElement.on("drop", onDropFrame);
        frameList.append(listElement);
        if (frameList.children().length > 1) {
            createButton.removeClass("disabled");
        }
    }
    
    function onCreateButtonClicked(event) {
        EventBus.dispatch("createGIF", getImages());
    }
 
    function getImages() {
        var images = [];
        frameList.find("img").each(function () {
            this.width = this.getAttribute("width");
            this.height = this.getAttribute("height");
            images.push(this);
        });
        return images;
    }

    function ondeleteFrameButtonClicked(event) {
        var parent = $(event.target).parent();
        parent.remove();
    }

    function init() {
        frameList = container.find(".frames");
        hiddenCanvas = container.find(".canvas");
        frameList.on("click", ".button.delete-frame", ondeleteFrameButtonClicked);
        createButton.on("click", onCreateButtonClicked);
    }

    init();

    return {
        addFrame: addFrame
    };

};
