var App = App || {};
App.ShowcaseView = function (container) {
    "use strict";
    /* eslint-env browser, jquery */

    function setShowcaseImage(url) {
        var image = $("<img class='gif' src='" + url + "'/>");
        container.empty();
        container.append(image);
    }

    return {
        setShowcaseImage: setShowcaseImage
    };

};
