var App = App || {};
App.GifModel = function (options) {
    "use strict";
    /* eslint-env browser */
    /* globals GIF */

    var imageList;

    function addImage(image, newDelay) {
        var delay = newDelay || options.delay;
        if (image instanceof Array) {
            image.forEach(addImage, newDelay);
        } else {
            imageList.push({
                image: image,
                delay: delay
            });
        }
    }

    function createGif(callback) {
        var gifEncoder = new GIF({
            workers: options.workers,
            quality: options.quality,
            workerScript: options.workerScript,
            width: options.width,
            height: options.height
        });
        imageList.forEach(function (frame) {
            gifEncoder.addFrame(frame.image, frame.delay);
        });
        gifEncoder.on("finished", function (blob) {
            callback(URL.createObjectURL(blob));
            imageList = [];
        });
        gifEncoder.render();
    }

    function init() {
        imageList = [];
    }

    init();
    return {
        addImage: addImage,
        createGif: createGif
    };

};
