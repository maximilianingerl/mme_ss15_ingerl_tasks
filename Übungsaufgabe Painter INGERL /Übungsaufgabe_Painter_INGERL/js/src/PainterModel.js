var App = App || {};

App.Model = function () {
    "use strict";
    /* globals EventBus */

    var COLOR_BLACK = "rgba(0,0,0,1)",
        COLOR_WHITE = "rgba(255,255,255,1)",
        COLOR_RED = "rgba(255,0,0,1)",
        COLOR_GREEN = "rgba(0,225,0,1)",
        COLOR_BLUE = "rgba(0,0,255,1)",
        COLOR_YELLOW = "rgba(255,255,0,1)",
        SIZE_SMALL_RADIUS = 15,
        SIZE_LARGE_RADIUS = 30,
        SIZE_SMALL_WIDTH = 5,
        SIZE_LARGE_WIDTH = 10,

        options = {
            tool: undefined,
            color: undefined,
            radius: undefined,
            width: undefined
        };

    function notifyAll() {
        EventBus.dispatch("optionsChanged", this, options);
    }

    function setTool(tool) {
        switch (tool) {
        case "circle":
            options.tool = "circle";
            break;
        case "square":
            options.tool = "square";
            break;
        case "line":
            options.tool = "line";
            break;
        case "eraser":
            options.tool = "eraser";
            break;
        }

        notifyAll();
    }

    function setColor(color) {
        switch (color) {
        case "black":
            options.color = COLOR_BLACK;
            break;
        case "white":
            options.color = COLOR_WHITE;
            break;
        case "red":
            options.color = COLOR_RED;
            break;
        case "green":
            options.color = COLOR_GREEN;
            break;
        case "blue":
            options.color = COLOR_BLUE;
            break;
        case "yellow":
            options.color = COLOR_YELLOW;
            break;
        }
        notifyAll();
    }

    function setSize(size) {
        switch (size) {
        case "large":
            options.radius = SIZE_LARGE_RADIUS;
            options.width = SIZE_LARGE_WIDTH;
            break;
        case "small":
            options.radius = SIZE_SMALL_RADIUS;
            options.width = SIZE_SMALL_WIDTH;
            break;
        }
        notifyAll();
    }

    function setDefaults(tool, color, size) {
        setTool(tool);
        setColor(color);
        setSize(size);
    }

    return {
        setTool: setTool,
        setColor: setColor,
        setSize: setSize,
        setDefaults: setDefaults
    };

};
