var App = App || {};

App.Palette = function (el) {
    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */

    var $palette;

    function setSelectedElement(element) {
        $palette.children().removeClass("selected");
        element.addClass("selected");
    }

    function onElementClicked(event) {
        var $target = $(event.target),
            color;

        color = $target.attr("class").replace("color", "").replace("selected", "").trim();
        setSelectedElement($target);
        EventBus.dispatch("colorSelected", this, {
            color: color
        });
    }

    function selectColor(color) {
        var $element = $palette.children("." + color);
        $element.trigger("click");
    }

    function init() {
        $palette = $(el);
        $palette.children(".color").on("click", onElementClicked);
    }

    init();

    return {
        selectColor: selectColor
    };
};
