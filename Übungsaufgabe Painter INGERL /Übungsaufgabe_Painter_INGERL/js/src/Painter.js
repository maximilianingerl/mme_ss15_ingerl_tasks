var App = App || {};
App.Painter = (function () {
    "use strict";
    /* eslint-env browser, jquery  */
    /* globals EventBus */

    var toolbox,
        palette,
        canvas,
        model;

    function saveImage() {
        var url = canvas.getDataUrl();
        window.open(url);
    }
    
    function clearCanvas() {
        canvas.clearCanvas();
    };
    
    function undoDrawing() {
        canvas.undo();
    };


    function initEvents() {
        EventBus.addEventListener("toolSelected", function (target, params) {
            model.setTool(params.tool);
            model.setSize(params.size);
        });

        EventBus.addEventListener("colorSelected", function (target, params) {
            model.setColor(params.color);
        });

        EventBus.addEventListener("optionsChanged", function (target, params) {
            canvas.setOptions(params);
        });

        EventBus.addEventListener("actionSelected", function (target, params) {
            switch (params.action) {
            case "save":
                saveImage();
                break;
            case "new":
                clearCanvas();
                break;
            case "undo":
                undoDrawing();
                break;
            }
        });
    }
    

    function init() {
        initEvents();
        toolbox = new App.Toolbox("#toolbox");
        palette = new App.Palette("#colors");
        canvas = new App.Canvas("#canvas");
        model = new App.Model();
        model.setDefaults("circle", "black", "large");
        toolbox.selectTool("circle", "large");
        palette.selectColor("black");
    }

    return {
        init: init
    };
}());
