var App = App || {};

App.Canvas = function (el) {
    "use strict";
    /* eslint-env jquery */

    var canvas,
        context,
        color,
        tool,
        radius,
        width,
        steps = [],
        mouse = {
            lastX: -1,
            lastY: -1,
            down: false
        };

    function getDataUrl() {
        return canvas.toDataURL();
    }

    function setOptions(options) {
        color = options.color;
        tool = options.tool;
        radius = options.radius;
        width = options.width;
    }

    function undo() {
        if (steps.length > 0) {
            context.clearRect(0, 0, canvas.width, canvas.height);
            steps.pop();
            var newCanvas = steps[steps.length - 1],
                img = new Image();
            img.src = newCanvas;
            context.drawImage(img, 0, 0);
        }
    }

    function drawLine(start, end) {
        if (start.x === -1 || start.y === -1) {
            return;
        }
        context.beginPath();
        context.moveTo(start.x, start.y);
        context.lineTo(end.x, end.y);
        context.strokeStyle = color;
        context.lineWidth = width;
        context.stroke();
        context.closePath();
    }

    function drawCircle(center) {
        context.beginPath();
        context.fillStyle = color;
        context.arc(center.x, center.y, radius, 0, 2 * Math.PI, false);
        context.fill();
        context.closePath();
    }

    function drawRect(center) {
        context.beginPath();
        context.fillStyle = color;
        context.rect(center.x - radius, center.y - radius, radius * 2, radius * 2);
        context.fill();
        context.closePath();
    }

    function clear(center) {
        context.clearRect(center.x - radius, center.y - radius, radius * 2, radius * 2);
    }

    function updateMouseInformation(x, y, clicked) {
        mouse.lastX = x;
        mouse.lastY = y;
        mouse.down = clicked;
    }

    function onMouseMoved(event) {
        if (!mouse.down) {
            return;
        }
        switch (tool) {
        case "line":
            drawLine({
                x: mouse.lastX,
                y: mouse.lastY
            }, {
                x: event.offsetX,
                y: event.offsetY
            });
            break;
        case "eraser":
            clear({
                x: event.offsetX,
                y: event.offsetY
            });
            break;
        }
        updateMouseInformation(event.offsetX, event.offsetY, mouse.down);
    }

    function onMouseLeft() {
        updateMouseInformation(-1, -1, false);
    }

    function onMouseDown(event) {
        updateMouseInformation(event.offsetX, event.offsetY, true);
        switch (tool) {
        case "circle":
            drawCircle({
                x: event.offsetX,
                y: event.offsetY
            });
            break;
        case "square":
            drawRect({
                x: event.offsetX,
                y: event.offsetY
            });
            break;
        }
    }

    function onMouseUp(event) {
        updateMouseInformation(event.offsetX, event.offsetY, false);
        steps.push(canvas.toDataURL());
    }


    function initEvents() {
        canvas.addEventListener("mousemove", onMouseMoved, false);
        canvas.addEventListener("mouseleave", onMouseLeft, false);
        canvas.addEventListener("mousedown", onMouseDown, false);
        canvas.addEventListener("mouseup", onMouseUp, false);
    }
    
    function clearCanvas() {
        context.clearRect(0, 0, canvas.width, canvas.height);
        steps = [];
    }


    function init() {
        canvas = $(el)[0];
        context = canvas.getContext("2d");
        initEvents();
    }

    init();

    return {
        init: init,
        clearCanvas: clearCanvas,
        undo: undo,
        getDataUrl: getDataUrl,
        setOptions: setOptions,
        undo: undo
    };
};
