var App = App || {};

App.Toolbox = function (el) {
    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */

    var $toolbox;


    function setSelectedElement(element) {
        $toolbox.children().removeClass("selected");
        element.addClass("selected");
    }

    function onElementClicked(event) {
        var $target = $(event.target),
            $parent = $target.parent(),
            tmp, item, size;
        if ($parent.hasClass("item")) {
            $target = $parent;
        }

        tmp = $target.attr("class").replace("item", "").replace("tool", "").replace("action", "").replace("selected", "").trim();
        item = tmp.split(" ")[0];
        size = tmp.split(" ")[1];
        if ($target.hasClass("tool")) {
            setSelectedElement($target);
            EventBus.dispatch("toolSelected", this, {
                tool: item,
                size: size
            });
        } else {
            EventBus.dispatch("actionSelected", this, {
                action: item
            });
        }
    }

    function selectTool(tool, size) {
        var $element = $toolbox.children("." + tool + "." + size);
        $element.trigger("click");
    }

    function init() {
        $toolbox = $(el);
        $toolbox.children(".item").on("click", onElementClicked);
    }

    init();

    return {
        selectTool: selectTool
    };
};
