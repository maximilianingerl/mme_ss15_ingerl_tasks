var App = App || {};
App.Mensaplan = (function () {
    "use strict";
    /* eslint-env browser, jquery  */
    var model = App.DataModel,
        view = App.ViewController;

    function onFavStatusChanged(itemName) {
        model.newFav(itemName);
    }

    function update() {
        model.update(view.update);
    }

    function init() {
        document.querySelector("header .button").addEventListener("click", update);
        model.restore(view.update);
        view.setFavCallback(onFavStatusChanged);
    }

    return {
        init: init
    };
}());
