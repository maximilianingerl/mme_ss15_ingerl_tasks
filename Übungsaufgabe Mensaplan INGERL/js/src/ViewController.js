var App = App || {};
App.ViewController = (function () {
    "use strict";
    /* eslint-env browser, jquery  */
    var favCallback,
        model = App.DataModel;

    function setFavCallback(callback) {
        favCallback = callback;
    }

    function onFavIconClicked(event) {
        event.target.classList.toggle("fa-circle");
        event.target.classList.toggle("fa-circle-thin");
        favCallback(event.target.parentElement.firstElementChild.innerHTML);
    }

    function addEntriesForDay(day, entries) {
        var dayContainer = document.querySelector("#week ul[day=" + day + "]");
        entries.forEach(function (entry) {
            var dayElement = document.createElement("li"),
                nameElement = document.createElement("span"),
                favElement = document.createElement("i");
            nameElement.classList.add("name");
            nameElement.innerHTML = entry.name;
            favElement.classList.add("fa");
            if(model.checkFav(entry.name) === false) {
            favElement.classList.add("fa-circle-thin");
            }
            if(model.checkFav(entry.name) === true) {
                favElement.classList.add("fa-circle");
            }
            favElement.addEventListener("click", onFavIconClicked);
            dayElement.classList.add(entry.category);
            dayElement.appendChild(nameElement);
            dayElement.appendChild(favElement);
            dayContainer.appendChild(dayElement);
        });
    }

    function clearEntries() {
        var currentEntries = document.querySelectorAll("#week li");
        for (var i = 0; i < currentEntries.length; ++i) {
            currentEntries[i].remove();
        }
    }

    function update(menuData) {
        clearEntries();
        for (var key in menuData) {
            if (typeof menuData[key] !== "function") {
                addEntriesForDay(key, menuData[key]);
            }
        }
    }

    return {
        update: update,
        setFavCallback: setFavCallback
    };
}());
