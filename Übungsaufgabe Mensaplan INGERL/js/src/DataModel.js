var App = App || {};
App.DataModel = (function () {
    "use strict";
    /* eslint-env browser, jquery  */
    var REQUEST_URL = "http://132.199.139.24:9001/mensa/uni/",
        DAYS = ["mo", "di", "mi", "do", "fr"],
        LOCAL_KEY = "mensa_speiseplan",
        FAVOURITE_KEY= "Favorisierte_Speisen",
        favourites = [],
        menu = {};

    function restoreFromLocalStorage(callback) {
        menu = JSON.parse(localStorage.getItem(LOCAL_KEY)) || {};
        favourites = JSON.parse(localStorage.getItem(FAVOURITE_KEY)) || [];
        callback(menu);
    }

    function saveToLocalStorage() {
        localStorage.setItem(LOCAL_KEY, JSON.stringify(menu));
    }
    
    function checkIfFavourite(entryName) {
        var favouriteStatus = false;
        for(var i=0; i<favourites.length;i++) {
            if(entryName == favourites[i]) {
                favouriteStatus = true;
            }
        }
        
        return favouriteStatus;
    }
    
    
    function saveFavToLocalStorage(fav) {
        checkIfAlreadyFavored(fav);
        localStorage.setItem(FAVOURITE_KEY, JSON.stringify(favourites));
    }
    
    function checkIfAlreadyFavored(newFavourite) {
        var alreadyFavored = false;
        var arrayIndex = 0;
            for(var i=0; i<favourites.length;i++) {
                if(favourites[i] == newFavourite) {
                    alreadyFavored = true;  
                    arrayIndex = i;
                }
            }
            if(alreadyFavored == false) {
                favourites.push(newFavourite);
            }else{
                favourites.splice(arrayIndex,1);
            }
    }
    

    function processServerResponse(day, callback, data) {
        data.forEach(function (item) {
            switch (item.category.charAt(0)) {
            case "S":
                item.category = "suppe";
                break;
            case "H":
                item.category = "hauptgericht";
                break;
            case "B":
                item.category = "beilage";
                break;
            case "N":
                item.category = "dessert";
                break;
            default:
                item.category = "unbekannt";
            }

        });
        menu[day] = data;
        if (Object.keys(menu).length === DAYS.length) {
            saveToLocalStorage();
            callback(menu);
        }
    }

    function requestServerData(day, callback) {
        $.ajax({
            type: "GET",
            url: REQUEST_URL + day,
            dataType: "json",
            success: processServerResponse.bind(this, day, callback)
        });
    }

    function update(callback) {
        DAYS.forEach(function (day) {
            requestServerData(day, callback);
        });
    }

    return {
        update: update,
        restore: restoreFromLocalStorage,
        newFav: saveFavToLocalStorage,
        checkFav: checkIfFavourite
    };
}());
