/**
 * @fileOverview ColorClicker GameController
 * @author Alexander Bazo <alexanderbazo@googlemail.com>
 * @version 1.1
 */

/**
 * @namespace ColorClicker
 */
var ColorClicker = ColorClicker || {};

/**
 * @namespace GameController
 * @memberof ColorClicker
 */
ColorClicker.GameController = (function () {
    "use strict";
    /* eslint-env browser */
    var config,
        currentLevel,
        username = "",
        view;

    /**
     * @private
     * @function loadHighscore
     * @memberof ColorClicker.GameController
     * @description Loads the current highscore from localstorage into the highscore member
     */
    function loadHighscore() {
        return localStorage.getItem(config.highscoreStorageKey);
    }

    /**
     * @private
     * @function saveHighscore
     * @memberof ColorClicker.GameController
     * @description Saves the current highscore from the currentLevel member to localstorage and the highscore member
     */
    function saveHighscore() {
        var highscore = loadHighscore();
        if (currentLevel > highscore) {
            localStorage.setItem(config.highscoreStorageKey, currentLevel);
            view.showUsernameInput();
        }
    }
    
    
    
    function getHighscoreList(newHighscore) {
        var USERNAME = username,
            SCORE = newHighscore,
            url = "http://132.199.139.24:9999/colorclicker/set/highscore/" + USERNAME + "/" + SCORE;
        
        $.ajax({
            url: url,
            type: "GET",
            contentType: "text/javascript",
            dataType: "json",
            success: function (json) {
                getTopTen(json);
            }
        });
	}
    
    function getTopTen(data) {
        var topTenScores = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        var topTenUser = ["", "", "", "", "", "", "", "", "", ""];
        var topTen = [];
        for (var i = 0; i<data.highscore.length; i++) {
                var alreadyInserted = false;
                for(var y = 0; y<topTenScores.length; y++){
                    if(data.highscore[i].score > topTenScores[y] && alreadyInserted == false) {
                        topTenScores.pop();
                        topTenScores.splice(y,0,data.highscore[i].score);
                        topTenUser.pop();
                        topTenUser.splice(y,0,data.highscore[i].user);
                        alreadyInserted = true;
                    }
                }
            }
        for( var z = 0; z<10; z++) {
            var obj = {
                'score': topTenScores[z],
                'user': topTenUser[z]       
            };
            topTen.push(obj);
            }
            view.showHighscoreList(topTen);
    }

    /**
     * @private
     * @function initGame
     * @memberof ColorClicker.GameController
     * @description Initializes a new game round
     * @param {Number} startLevel Level-Index to start with
     * @param {Number} boxCount Number of boxes displayed in this round
     * @param {Object} color Color used to draw the boxes in this round
     * @param {Number} deviation Color deviation for this round
     */
    function initGame(startLevel, boxCount, color, deviation) {
        var highscore = loadHighscore();
        view.clearBoxes();
        view.setScore(startLevel, highscore);
        view.addBoxes(boxCount, color, deviation);
    }

    /**
     * @function startNextRound
     * @memberof ColorClicker.GameController
     * @description Starts a new round by initializing the next level
     */
    function startNextRound() {
        var boxCount = config.defaultBoxCount,
            color = ColorClicker.Color.getRandomColor(),
            deviation = config.defaultBoxDeviation - (currentLevel * config.deviationFactor);

        currentLevel++;

        if (currentLevel < config.boxesPerLevel.length) {
            boxCount = config.boxesPerLevel[currentLevel];
        } else {
            boxCount = config.boxesPerLevel[config.boxesPerLevel.length - 1];
        }

        if (deviation <= config.minimalDeviation) {
            deviation = config.minimalDeviation;
        }

        initGame(currentLevel, boxCount, color, deviation);
    }

    /**
     * @private
     * @function resetGame
     * @memberof ColorClicker.GameController
     * @description Resets the game by setting the current level to 0
     */
    function resetGame() {
        currentLevel = 0;
    }

    /**
     * @public
     * @function startNewGame
     * @memberof ColorClicker.GameController
     * @description Starts a new game by initializing the first level
     */
    function startNewGame() {
        var color = ColorClicker.Color.getRandomColor();
        saveHighscore();
        resetGame();
        initGame(currentLevel, config.defaultBoxCount, color, config.defaultBoxDeviation);
    }

    function onTargetBoxClicked() {
        startNextRound();
    }

    function onNonTargetBoxClicked() {
        view.revealTarget(startNewGame);
    }

    /**
     * @public
     * @function init
     * @memberof ColorClicker.GameController
     * @param {Object} gameConfig Configuration object
     * @param gameConfig.highscoreStorageKey Access key to get/store the highscore in local storage
     * @param gameConfig.defaultBoxDeviation Default rgb deviation between box color and target color
     * @param gameConfig.defaultBoxCount Default number of boxes
     * @param gameConfig.minimalDeviation Minimal color deviation
     * @param gameConfig.deviationFactor Factor used to calculate color deviation decrease for each level
     * @param gameConfig.boxesPerLevel Array with number of boxes for each level
     * @param {Object} viewController ViewController to be used by this module
     */
    function init(gameConfig, viewController) {
        var viewConfig = {
            boxesPerRow: 8,
            restartDelay: 2000,
            onTargetClicked: onTargetBoxClicked,
            onTargetMissed: onNonTargetBoxClicked,
            boardView: document.querySelector("#board"),
            scoreView: document.querySelector("#info .score .current"),
            highscoreView: document.querySelector("#info .score .highscore")
        };
        config = gameConfig;
        view = viewController;
        view.init(viewConfig);
        var inputField = document.getElementById("userInputField");
        inputField.addEventListener("keyup",_onInputKeyUp);
        var sendButton = document.getElementById("userInputButton");
        sendButton.addEventListener("click",_onClicked);
    };
    
    function _onInputKeyUp(keyEvent) {
        if(keyEvent.keyCode !== 16 && keyEvent.keyCode !== 32 && keyEvent.keyCode >=65 && keyEvent.keyCode <= 90 && username.length<=30) {
            username = username + String.fromCharCode(keyEvent.keyCode);
        };
        if(keyEvent.keyCode >= 48 && keyEvent.keyCode <= 57 && username.length<=30) {
            username = username + String.fromCharCode(keyEvent.keyCode);
        };
        if(keyEvent.keyCode === 8) {
            username = username.substring(0, username.length - 1);
        };
    };
    
    function _onClicked(clickEvent) {
        getHighscoreList(currentLevel);
        view.hideUsernameInput();
    };

    return {
        init: init,
        start: startNewGame,
    };
})();
